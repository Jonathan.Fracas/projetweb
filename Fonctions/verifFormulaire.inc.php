<?php

/**
 * Verification que le login est correct : 
 * - respecte le format impose
 * - login non existant
 * @param bool $valide prend la valeur false si le login est non valide
 * @param array $ChampsIncorrects tableau contenant les champs incorrects
 */
function verificationLogin(&$valide,&$ChampsIncorrects){
    if(!isset($_POST['login']) || !preg_match('/^[a-zA-Z0-9]+$/',$_POST['login']) ){
        array_push($ChampsIncorrects,"login : invalide");
        $valide =false;
    }
    else if(utilisateurExiste($_POST['login'])){
        array_push($ChampsIncorrects,"login : login deja existant");
        $valide =false;
    }
}


/**
 * Verification que le password est correct : 
 * - respecte le format impose
 * @param bool $valide prend la valeur false si le password est non valide
 * @param array $ChampsIncorrects tableau contenant les champs incorrects
 */
function verificationPassword(&$valide,&$ChampsIncorrects){
    if(!isset($_POST['password']) || !preg_match('/^.+$/',$_POST['password'])){
        $valide =false;
        array_push($ChampsIncorrects,"password : invalide");
    }
}

/**
 * Verification que le nom est correct : 
 * - respecte le format impose
 * @param bool $valide prend la valeur false si le nom est non valide
 * @param array $ChampsIncorrects tableau contenant les champs incorrects
 */
function verificationNom(&$valide,&$ChampsIncorrects){
    if(  (!empty($_POST['nom']) )){
        $nom = enleveaccents($_POST['nom']);
        if(!preg_match('/^([a-zA-Z]+((\'|-)?[a-zA-Z])+|([a-zA-Z])*[ ]*)+$/',$nom)){
            $valide =false;
            array_push($ChampsIncorrects,"nom : invalide");
        }
    }
}

/**
 * Verification que le prenom est correct : 
 * - respecte le format impose
 * @param bool $valide prend la valeur false si le prenom est non valide
 * @param array $ChampsIncorrects tableau contenant les champs incorrects
 */
function verificationPrenom(&$valide,&$ChampsIncorrects){
    if(  (!empty($_POST['prenom']))){
        $prenom = enleveaccents($_POST['prenom']);
        if(!preg_match('/^([a-zA-Z]+((\'|-)?[a-zA-Z])+|([a-zA-Z])*[ ]*)+$/',$prenom)){
            $valide =false;
            array_push($ChampsIncorrects,"prenom : invalide");
        }
    }
}

/**
 * Verification que le sexe est correct : 
 * - homme ou femme selectionne
 * @param bool $valide prend la valeur false si le sexe est non valide
 * @param array $ChampsIncorrects tableau contenant les champs incorrects
 */
function verificationSexe(&$valide,&$ChampsIncorrects){
    if(!isset($_POST['sexe']) ){
        $_POST['sexe'] = NULL;
	}
    else if((trim($_POST['sexe'])!='f' && (trim($_POST['sexe'])!='h'))){
        $valide = false;
        array_push($ChampsIncorrects,"sexe : veuillez selectionner homme ou femme");
    }
}


/**
 * Verification que la date de naissance est correct : 
 * - respecte le format impose
 * - la date est valide
 * - l'utilisateur a plus de 18 ans
 * @param bool $valide prend la valeur false si le date de naissance est non valide
 * @param array $ChampsIncorrects tableau contenant les champs incorrects
 */
function verificationDateDeNaissance(&$valide,&$ChampsIncorrects){
    if(!empty($_POST['dateNaissance'])){
        if(!preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/',$_POST['dateNaissance'])){
            $valide = false;
            array_push($ChampsIncorrects,"date de naissance : format invalide");   
        }
        else{
            $date = explode('/',$_POST['dateNaissance']);
            if(!checkdate($date[1],$date[0],$date[2])){
                $valide = false;
                array_push($ChampsIncorrects,"date de naissance : date incorrecte");   
            }
            else{
                $date= $date[2].'-'.$date[1].'-'.$date[0];
                if((new \DateTime())->diff(new \DateTime($date))->format('%y')<18 ){
                    $valide = false;
                    array_push($ChampsIncorrects,"date de naissance : vous n'avez pas 18 ans");  
                }
            }
        }
    }
}


?>