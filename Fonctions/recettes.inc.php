<?php

include "gestionUtilisateurs.inc.php";

/**
 * Fonction qui affiche la photo du cocktail si elle existe dans le dossier Photos.
 * @param string $titreRecette le titre de la recette.
 */
function affichagePhoto($titreRecette){
    $nom = str_replace(' ', '_',$titreRecette);
            $nom = strtolower($nom);
            $nom = ucfirst($nom);
            $nom.=".jpg";
            $nom = enleveaccents($nom);
            // Si la recette à une image associee dans le dossier Photos alors on l'affiche
            if(file_exists('Photos/'.$nom)){
                ?>
                <img src="Photos/<?php echo $nom; ?>" alt="" style="max-width : 200px; max-height : 200px;">
                <?php
            }
            else{
                ?>
                <img src="Images/DefaultCocktail.png" style="max-width : 200px; max-height : 200px;" alt="">
                <?php
            }

}

/**
 * Fonction qui affiche un coeur plein si la recette est dans les recettes preferees,
 * un coeur vide sinon.
 * 
 * @param int $indiceRecette l'indice de la recette
 */
function affichageCoeur($indiceRecette){
    ?>
    <form method="post" action="#">
    <input name="indice" type="hidden" value="<?php echo $indiceRecette;?>">
    <?php
    if($_SESSION['utilisateurconnecte']==""){
        if(!in_array($indiceRecette,$_SESSION['fav'])){
            ?>
            <input name="fav" type="hidden" value="<?php echo 'ajout';?>">
            <input  type="image" src="Images/CoeurVide.png" alt="" width="50" height="50" />
            <?php
        }
        else{
            ?>
            <input name="fav" type="hidden" value="<?php echo 'supp';?>">
            <input  type="image" src="Images/CoeurPlein.png" alt="" width="50" height="50" />
            <?php
        }
    }
    else{
        if(file_exists("BDD/".$_SESSION['utilisateurconnecte'].".json")){
            $fichier = getFichierUtilisateursRP();
            $contenu = file_get_contents($fichier);
            $recettesPreferees = json_decode($contenu, true);
            if(is_array($recettesPreferees) && array_key_exists($indiceRecette,$recettesPreferees)){
                ?>
                <input name="fav" type="hidden" value="<?php echo 'supp';?>">
                <input  type="image" src="Images/CoeurPlein.png" alt="" width="50" height="50"/>
                <?php
            }
            else{
                ?>
                <input name="fav" type="hidden" value="<?php echo 'ajout';?>">
                <input  type="image" src="Images/CoeurVide.png" alt="" width="50" height="50"/>
                <?php
            }
        }
        else{
            ?>
            <input name="fav" type="hidden" value="<?php echo 'supp';?>">
            <input  type="image" src="Images/CoeurVide.png" alt="" width="50" height="50" />
            <?php
        }
    }
    ?>
    </form>
    <?php
}

/**
 * Fonction qui permet de savoir si un utilisateur ajoute/supprime une recette des favoris.
 * Et effectue cette action.
 * @param array $Recettes le tableau de Recettes (Donnees.inc.php)
 */
function gestionFavoris($Recettes){
    if(isset($_POST['indice'])){
        switch($_POST['fav']){
            case 'ajout' : 
                ajoutFav($Recettes);
                break;
            case 'supp' :
                suppFav($Recettes); 
                break;
            default: 
                break;
        }
    }
}

/**
 * Ajoute une recette en favoris.
 * La recette est ajoutée aux favoris de session et aux favoris de l'utilisateur s'il est connecte.
 * @param array $Recettes le tableau de Recettes (Donnees.inc.php)
 */
function ajoutFav($Recettes){
    array_push($_SESSION['fav'],$_POST['indice']);
    asort($_SESSION['fav']);
    if($_SESSION['utilisateurconnecte']!=""){
        ajoutFavoriUtilisateur($_POST['indice'],$Recettes[$_POST['indice']]['titre']);
    }
}

/**
 * Supprime une recette des favoris.
 * La recette est supprimee des favoris de session et des favoris de l'utilisateur s'il est connecte.
 * @param array $Recettes le tableau de Recettes (Donnees.inc.php)
 */
function suppFav($Recettes){
    unset($_SESSION['fav'][array_search($_POST['indice'], $_SESSION['fav'])]);
    if($_SESSION['utilisateurconnecte']!=""){
        supprimeFavoriUtilisateur($_POST['indice']);
    }
}

/**
 * Fonction recursive 
 * @param array $array tableau de la sous-categorie d'une categorie.
 * @param array $hierarchie tableau de la hierarchie (Donnees.inc.php)
 * @param array $res le tableau contenant l'ensemble des indgredients d'une categorie et de ses sous categories.
 * 
 */
function trouverSousCategories($categorie,$Hierarchie,&$res) {
    if(isset($Hierarchie[$categorie]['sous-categorie'])){
        foreach($Hierarchie[$categorie]['sous-categorie'] as $sousCategorie){
            trouverSousCategories($sousCategorie,$Hierarchie,$res);
        }
        if(!in_array($categorie,$res)){
            array_push($res,$categorie);
        }
    }
    else{
        if(!in_array($categorie,$res)){
            array_push($res,$categorie);        
        }
    }
}

/**
 * Fonction qui analyse la chaine de caractere de la recherche
 * @param string $chaine la chaine.
 * 
 * @return bool|array renvoie faux si le nombre de guillemets est impaire | renvoie un tableau comprenant les ingredients de la recherche. 
 */
function analyseRecherche ($chaine){
    $nbGuillemets = substr_count($chaine, '"');
    if($nbGuillemets % 2 == 1){
        return false;
    }
    return preg_split('/[\s,]*([-|+]?\\"[^\\"]+\\")[\s,]*|'. '[\s,]+/', $chaine, 0,PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
}



/**
 * Fonction qui repartie dans 3 tableaux une liste d'ingredients.
 * @param array $liste tableau d'ingredients (de la recherche).
 * @param array $souhaite tableau contenant les ingredients souhaités : sortie.
 * @param array $nonSouhaite tableau contenant les ingredients non souhaités : sortie.
 * @param array $nonReconnus tableau contenant les ingredients non reconnus : sortie.
 * @param array $hierarchie tableau de la hierarchie (Donnees.inc.php)
 */
function repartitionRecherche($liste,&$souhaite,&$nonSouhaite,&$nonReconnus,$Hierarchie){
    $sousCategorieAliment =array(); 
    trouverSousCategories('Aliment',$Hierarchie,$sousCategorieAliment);
    foreach($liste as $ingredient){
        $ingredient = str_replace('"',"",$ingredient);
        if($ingredient[0]=='-'){
            $ingredient = substr($ingredient,1);
            if(in_array($ingredient,$sousCategorieAliment)){
                array_push($nonSouhaite,$ingredient);
            }
            else{
                array_push($nonReconnus,$ingredient);
            }
        }
        else{
            if($ingredient[0]=='+'){
               $ingredient = substr($ingredient,1);
            }
            if(in_array($ingredient,$sousCategorieAliment)){
                array_push($souhaite,$ingredient);
            }
            else{
                array_push($nonReconnus,$ingredient);
            }
        }
    }

}

/**
 * Fonction qui retourne le dernier indice d'un tableau.
 * @param array le tableau.
 * 
 * @
 */
function dernierIndiceTableau($array){
    end($array);
    return key($array);
}

?>

