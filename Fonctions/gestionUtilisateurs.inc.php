<?php 



/**
 * @return string Chemin relatif du fichier contenant la liste des utilisateurs
 */
function getFichierUtilisateurs() {
    if(file_exists("BDD/utilisateurs.json")){
        return "BDD/utilisateurs.json";
    }
    else{
        return "../BDD/utilisateurs.json";
    }
  }

/**
 * Recupere la liste de tous les utilisateurs
 * @return array tableau des utilisateurs
 */
function getUtilisateurs() {
  $contenu = file_get_contents(  getFichierUtilisateurs());
  return json_decode( $contenu, true );
}

/**
 * Recupère un seul utilisateur s'il existe
 * @param string $login login de l'utilisateur
 * @return array l'utilisateur
 */
function getUtilisateur( $login ) {
    $utilisateurs = getUtilisateurs();
    return $utilisateurs[$login];
}

/**
 * Fonction qui test si un utilisateur existe
 * @param string $login login de l'utilisateur
 * @return bool vrai si l'utilisateur existe, faux sinon
 */
function utilisateurExiste( $login ) {
    $utilisateurs = getUtilisateurs();
    if(array_key_exists($login, $utilisateurs)){
        return true;
    }
    else{
        return false;
    }
}

/**
 * Ajoute l'utilisateur dans le fichier 'utilisateurs.json'
 * Le login et le password sont les seules informations obligatoires.
 * Par defaut, les autres informations valent "".
 * 
 * @param string $login login de l'utilisateur
 * @param string $password Mot de passe de l'utilisateur
 * @param string $prenom prenom de l'utilisateur
 * @param string $nom nom de l'utilisateur 
 * @param string $dateNaissance (jj/mm/aaaa) date de naissance de l'utilisateur
 * @param string $sexe sexe de l'utilisateur
 * 
 */
function ajouterUtilisateur( $login, $password,$prenom,$nom,$dateNaissance,$sexe) {
    $utilisateurs = getUtilisateurs();

    $utilisateurs[$login] = [
        'password' => sha1( $password ),
        'prenom'    => $prenom,
        'nom'    => $nom,
        'date' => $dateNaissance,
        'sexe' => $sexe
    ];
    enregistrerUtilisateurs( $utilisateurs );
}

/**
 * Modifie un utilisateur dans le fichier 'utilisateurs.json'. 
 * Seule le login n'est pas modifiable.
 * 
 * @param string $login login de l'utilisateur
 * @param string $password Mot de passe de l'utilisateur
 * @param string $prenom prenom de l'utilisateur
 * @param string $nom nom de l'utilisateur 
 * @param string $dateNaissance (jj/mm/aaaa) date de naissance de l'utilisateur
 * @param string $sexe sexe de l'utilisateur
 * 
 */
function modifierUtilisateur( $login, $password,$prenom,$nom,$dateNaissance,$sexe) {
    $utilisateurs = getUtilisateurs();

    $utilisateurs[$login] = [
        'password' => sha1( $password ),
        'prenom'    => $prenom,
        'nom'    => $nom,
        'date' => $dateNaissance,
        'sexe' => $sexe
    ];
    enregistrerUtilisateurs( $utilisateurs );
}

/**
 * Enregistre un nouveau utilisateur ou met a jour un utilisateur existant dans le fichier 'utilisateurs.json'. 
 * @param array $utilisateurs tableau des utilisateurs
 */
function enregistrerUtilisateurs( $utilisateurs ) {
    $contenu = json_encode( $utilisateurs, JSON_PRETTY_PRINT );
    file_put_contents( getFichierUtilisateurs(), $contenu );
}

/**
 * Tente de connecter un utilisateur.
 * 
 * @param string $login login de l'utilisateur
 * @param string $password Mot de passe de l'utilisateur
 */
function connexion( $login, $password ) {
    if(!utilisateurExiste($login)){
        return false;
    }
    $utilisateur = getUtilisateur($login);
    if( $utilisateur['password'] !== sha1( $password ) ) {
        return false;
        }

    // Enregistrement des données dans la session de l'utilisateur
    $_SESSION['utilisateurconnecte'] = $login;
    unset($_SESSION['fav']);
    $_SESSION['fav']=array();

    // array_push($_SESSION['fav'],'');
    $recettesPreferees = getRecettesPrefereesUtilisateurs();
    foreach($recettesPreferees as $indice => $titre){
        array_push($_SESSION['fav'],$indice);
    }
    
    return true;
}

/**
 * Fonction qui remplace les caracteres accentues par les mêmes caracteres
 * non accentues
 * @param string $str la chaine de caractere initiale
 * @return string la chaine initiale sans accents.
 */
function enleveaccents($str) {
    $TB_CONVERT = array(
        'Š' => 'S', 'š' => 's', 'Ð' => 'Dj', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A',
        'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I',
        'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U',
        'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a',
        'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i',
        'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u',
        'ú' => 'u', 'û' => 'u', 'ü'=>'u','ý' => 'y', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'ƒ' => 'f', 'œ' => 'oe'
    );
    $res = strtr($str, $TB_CONVERT); 
    return $res;
}


/**
 * @return string Chemin relatif du fichier contenant les recettes preferees d'un utilisateur
 */
function getFichierUtilisateursRP() {
    if(file_exists("../BDD/".$_SESSION['utilisateurconnecte'].".json")){
        return "../BDD/".$_SESSION['utilisateurconnecte'].".json";
    }
    else{
        return "BDD/".$_SESSION['utilisateurconnecte'].".json";
    }
  }

function getRecettesPrefereesUtilisateurs(){
    if(file_exists(getFichierUtilisateursRP())){
        $contenu = file_get_contents(getFichierUtilisateursRP());
        $recettesPreferees = json_decode($contenu, true);
    }
    else{
        $recettesPreferees =array();
    }
    return $recettesPreferees;
}

/**
 * Fonction qui ecrit les recettes preferees d'un utilisateur dans son fichier.
 * 
 * @param array $recettesPreferees les recettes preferees d'un utilisateur
 */
function setRecettesPrefereesUtilisateurs($recettesPreferees){
    $contenu_json = json_encode($recettesPreferees, JSON_PRETTY_PRINT);
    file_put_contents(getFichierUtilisateursRP(), $contenu_json);
}

/**
 * Fonction qui ajoute une recette aux recettes preferees d'un utilisateur.
 * 
 * @param int $indice indice de la recette
 * @param string $titre titre de la recette
 */
function ajoutFavoriUtilisateur($indice,$titre){
    $recettesPreferees[$indice] = $titre;
    if(!file_exists(getFichierUtilisateursRP())){
        setRecettesPrefereesUtilisateurs($recettesPreferees);
    }
    else{
        $recettesPreferees = getRecettesPrefereesUtilisateurs();
        $recettesPreferees[$indice] = $titre;
        asort($recettesPreferees);
        setRecettesPrefereesUtilisateurs($recettesPreferees);
    }
}

/**
 * Fonction qui supprime une recette des recettes preferees d'un utilisateur.
 * 
 * @param int $indice indice de la recette
 * @param string $titre titre de la recette
 */
function supprimeFavoriUtilisateur($indice){
    $recettesPreferees = getRecettesPrefereesUtilisateurs();
    unset($recettesPreferees[$indice]);
    setRecettesPrefereesUtilisateurs($recettesPreferees);
}
?>