<?php
include "Donnees.inc.php";

/*
  Creation du Fil d'Ariane contenant Aliment la super categorie.  
*/
if(!isset($_SESSION['ariane'])){
    $_SESSION['ariane'] = array('Aliment');
}

/*
  Modification du fil d'Ariane quand l'utilisateur selectionne une categorie.
  Deux cas possibles : 
  - L'utlisateur a clique sur une categorie du fil d'Ariane, dans ce cas on supprime toutes les categories qui se trouvent
    apres celle-ci dans le tableau contenant le fil.
  - L'utilisateur a clique sur une sous categorie de la derniere categorie du fil, dans ce cas on ajoute la categorie au fil.   
*/
if(isset($_GET['categorie']) && isset($Hierarchie[$_GET['categorie']])){
    if(in_array($_GET['categorie'],$_SESSION['ariane'])){
        $indice = array_search($_GET['categorie'],$_SESSION['ariane']);
        $taille = count($_SESSION['ariane']);
        for($i = $indice+1; $i < $taille; $i++){
            unset($_SESSION['ariane'][$i]);
        }
    }
    else{
        array_push($_SESSION['ariane'],$_GET['categorie']);
    } 
}

?>
<h2>Aliment courant</h2>
<?php
// Affichage du fil d'Ariane.
$dernierIndice = dernierIndiceTableau($_SESSION['ariane']);
foreach($_SESSION['ariane'] as $indice => $categorie){
    if($dernierIndice === $indice){
        echo '<a href="?page=listeRecettes&categorie='.$categorie.'">'.$categorie.'</a>';
    }
    else{
        echo '<a href="?page=listeRecettes&categorie='.$categorie.'">'.$categorie.'</a>'.' /';
    }
}

?>

<h2>Sous-categorie</h2>
<div class='sousCategorie'>
<?php
    // Affichage des sous categories de la categorie selectionnee par defaut la categorie est 'Aliment'.
    if(isset($_GET['categorie']) && isset($Hierarchie[$_GET['categorie']])){
        if(array_key_exists('sous-categorie',$Hierarchie[$_GET['categorie']])){
            foreach($Hierarchie[$_GET['categorie']]['sous-categorie'] as $sousCategorie ){
                echo '<ul><a class="text-dark" href="?page=listeRecettes&categorie='.$sousCategorie.'">'.$sousCategorie.'</a></ul>';
            }
        }
    }
    else{
        foreach($Hierarchie['Aliment']['sous-categorie'] as $sousCategorie ){
            echo '<ul><a class="text-dark" href="?page=listeRecettes&categorie='.$sousCategorie.'">'.$sousCategorie.'</a></ul>';
        }
    }  
?>
</div>