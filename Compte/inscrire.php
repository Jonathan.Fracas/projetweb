<?php

include "Fonctions/gestionUtilisateurs.inc.php";
include "Fonctions/verifFormulaire.inc.php";


$valide = false;
// Vérification du formulaire

if(isset($_POST['submit']))  // le formulaire vient d'être soumis
  { 
    $ChampsIncorrects=array();
    $valide=true;
    
    verificationLogin($valide,$ChampsIncorrects);
    verificationPassword($valide,$ChampsIncorrects);
    verificationNom($valide,$ChampsIncorrects);
    verificationPrenom($valide,$ChampsIncorrects);
    verificationSexe($valide,$ChampsIncorrects);
    verificationDateDeNaissance($valide,$ChampsIncorrects);

    if($valide){
        ajouterUtilisateur($_POST['login'],$_POST['password'],$_POST['prenom'],$_POST['nom'],$_POST['dateNaissance'],$_POST['sexe']);
        connexion($_POST['login'],$_POST['password']);
    }
}
?>

<nav>

</nav>

<main>

<?php	

if(!$valide) {
?>
    <h1>Inscription</h1>
    <form method="post" action="#" >
    <fieldset>
        Login :    
    	<input type="text" name="login" required="required" placeholder="champ obligatoire"
    		   value="<?php if(isset($_POST['login']))  echo $_POST['login']; ?>"/>
    	<br />
        Mot de passe:      
    	<input type="password" name="password" required="required" placeholder="champ obligatoire"
    		   value="<?php if(isset($_POST['password']))  echo $_POST['password']; ?>"/>
    	<br />
        Nom :    
    	<input type="text" name="nom"
    		   value="<?php if(isset($_POST['nom']))  echo $_POST['nom']; ?>"/>
    	<br />   
        Prénom : 
    	<input type="text" name="prenom" 
    	       value="<?php if(isset($_POST['prenom']))  echo $_POST['prenom']; ?>"/>
    	<br /> 
    	Vous êtes :  
      	<input type="radio" name="sexe" value="f" 
    	<?php if((isset($_POST['sexe']))&&($_POST['sexe'])=='f') echo 'checked="checked"'; ?>
    	/> une femme 	
    	<input type="radio" name="sexe" value="h"
    	<?php if((isset($_POST['sexe']))&&($_POST['sexe'])=='h') echo 'checked="checked"'; ?>
    	/> un homme	
    	<br />	
        Date de naissance : 
    	<input type="text" name="dateNaissance" placeholder="format jj/mm/aaaa"
    	       value="<?php if(isset($_POST['dateNaissance'])) echo $_POST['dateNaissance']; ?>"/>
        <br /> 	
    </fieldset>  
    	<br />
    <input type="submit" name="submit" value="Valider" />
    </form>
    
    <?php 
    if(isset($_POST['submit']))  {
        ?>
        <label>Champ(s) incorrect(s) </label>
        <?php
        foreach($ChampsIncorrects as $champ){
            ?>
            <ul><?php echo $champ ;?></ul>
            <?php
        }
    }
}
else{
    header('location: index.php');
}

?>

</main>