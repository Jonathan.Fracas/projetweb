<?php

include "Fonctions/gestionUtilisateurs.inc.php";
include "Fonctions/verifFormulaire.inc.php";


$utilisateur = getUtilisateur($_SESSION['utilisateurconnecte']);

$valide = true;
// Vérification du formulaire

if(isset($_POST['submit']))  // le formulaire vient d'être soumis
  { 
    $ChampsIncorrects=array();
    
    if(!empty($_POST['password'])){
        verificationPassword($valide,$ChampsIncorrects);
    }
    verificationNom($valide,$ChampsIncorrects);
    verificationPrenom($valide,$ChampsIncorrects);
    verificationSexe($valide,$ChampsIncorrects);
    verificationDateDeNaissance($valide,$ChampsIncorrects);

    if($valide){
        modifierUtilisateur($_SESSION['utilisateurconnecte'],$_POST['password'],$_POST['prenom'],$_POST['nom'],$_POST['dateNaissance'],$_POST['sexe']);
        header("Refresh: 0;");
    }
}

?>

<nav>
    
</nav>

<main>


<h1>Profil</h1>
    <form method="post" action="#" >
    <fieldset>
        Login : 
        <label><?php echo $_SESSION['utilisateurconnecte'] ?></label>  
    	<br />
        Mot de passe:      
    	<input type="password" name="password"
    		   />
    	<br />
        Nom :    
    	<input type="text" name="nom"
    		   value="<?php echo $utilisateur['nom']; ?>"/>
    	<br />   
        Prénom : 
    	<input type="text" name="prenom" 
    	       value="<?php echo $utilisateur['prenom'];  ?>"/>
    	<br /> 
    	Vous êtes :  
      	<input type="radio" name="sexe" value="f" 
    	<?php if(($utilisateur['sexe'])=='f') echo 'checked="checked"'; ?>
    	/> une femme 	
    	<input type="radio" name="sexe" value="h"
    	<?php if(($utilisateur['sexe'])=='h') echo 'checked="checked"'; ?>
    	/> un homme	
    	<br />	
        Date de naissance : 
    	<input type="text" name="dateNaissance" placeholder="format jj/mm/aaaa"
    	       value="<?php echo $utilisateur['date']; ?>"/>
        <br /> 	
    </fieldset>  
    	<br />
    <input type="submit" name="submit" value="Modifier" />
    </form>
    <?php 
    if(!$valide)  {
        ?>
        <label>Champ(s) incorrect(s) </label>
        <?php
        foreach($ChampsIncorrects as $champ){
            ?>
            <ul><?php echo $champ ;?></ul>
            <?php
        }
    }

?>

</main>