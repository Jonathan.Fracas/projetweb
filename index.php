<?php
  session_start();



  if(!isset($_GET["page"])){
    $_GET["page"]="listeRecettes";
  }

  if(!isset($_SESSION['utilisateurconnecte'])){ 
    $_SESSION['utilisateurconnecte']="";
}

// Creation d'un tableau contenant les recettes preferees d'un utilisateur non connecte
if(!isset($_SESSION['fav'])){
    $_SESSION['fav'] = array();
}

?>
<!DOCTYPE html>
<html>

<head>
    <title>Cocktails</title>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"> </script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet"  href="style.css" type="text/css"  media="screen" />
</head>

<body>
    
<header>
    <?php
        include('header.php');
    ?>
</header>

<?php

switch ($_GET["page"])
    {
        case "recette" :
            include 'Recettes/recette.php';
            break;
        case "recettePreferees" :
            include 'Recettes/recettePreferees.php';
            break;
        case "listeRecettes" :
            include 'Recettes/listeRecettes.php';
            break;
        case "inscrire" :
            include 'Compte/inscrire.php';
            break;
        case "profil" :
            include 'Compte/profil.php';
            break;
        case "rechercheRecettes" :
            include 'Recettes/rechercheRecettes.php';
            break;

        default :
            include 'Recettes/listeRecettes.php';
     }

?>

<footer></footer>

</body>
</html>

<script language=javascript>

    document.addEventListener("DOMContentLoaded", function (event) {
        var scrollpos = sessionStorage.getItem('scrollpos');
        if (scrollpos) {
            window.scrollTo(0, scrollpos);
            sessionStorage.removeItem('scrollpos');
        }
    });

    window.addEventListener("beforeunload", function (e) {
        sessionStorage.setItem('scrollpos', window.scrollY);
    });

</script>