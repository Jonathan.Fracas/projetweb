<?php
include "Donnees.inc.php";
include "Fonctions/recettes.inc.php";

gestionFavoris($Recettes);



?>


<nav>
    <?php
        include 'Navigation/hierarchie.php';
    ?>
</nav>

<main>

        <h2>Recettes preferees </h2>
        <div id="recettes">
        <?php
        if($_SESSION['utilisateurconnecte']==""){
            foreach($_SESSION['fav'] as  $indice){
        ?>
                <div id="recette">
                     <div id="nomFav">
        <?php         
                echo '
                     <ul><a href="?page=recette&recette='.$indice.'">'.$Recettes[$indice]['titre'].' </a></ul>';
                affichageCoeur($indice);
        ?>
                      </div>
                            <div style="text-align: center;">
        <?php
                affichagePhoto($Recettes[$indice]['titre']);
        ?>
                            </div>
        <?php
                foreach($Recettes[$indice]['index'] as $ingredient){
        ?> 
                    <ul><?php  echo $ingredient;?></ul>
        <?php
                }
        ?>
                </div>
        <?php 
            }
        }    
        else{
            $recettesPreferees = getRecettesPrefereesUtilisateurs();
            if(!empty($recettesPreferees)){
                foreach($recettesPreferees as $indice => $titre){
                        ?>
                                <div id="recette">
                                     <div id="nomFav">
                        <?php         
                                echo '<ul><a href="?page=recette&recette='.$indice.'">'.$titre.' </a></ul>';
                                affichageCoeur($indice);
                        ?>
                                      </div>
                                            <div style="text-align: center;">
                        <?php
                                affichagePhoto($titre);
                        ?>
                                            </div>
                        <?php
                                foreach($Recettes[$indice]['index'] as $ingredient){
                        ?> 
                                    <ul><?php  echo $ingredient;?></ul>
                        <?php
                                }
                        ?>
                                </div>
                <?php
                }
            }
        ?>
        </div>
        <?php
        }
        ?>
        
</main>