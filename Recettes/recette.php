<?php
include "Donnees.inc.php";
include "Fonctions/recettes.inc.php";

gestionFavoris($Recettes);
?>

<nav>
    <?php
        include 'Navigation/hierarchie.php';
    ?>
</nav>

<main>

<?php
if(isset($_GET['recette'])){
 ?>
    <div id ="nomRecette_coeur">
    <h1><?php echo $Recettes[$_GET['recette']]['titre'] ?></h1>
        <div id="coeur">
<?php
    affichageCoeur($_GET['recette']);
?>
        </div>
    </div>
<?php
    affichagePhoto($Recettes[$_GET['recette']]['titre']);
    ?>
     <h2>Liste d'ingredients : </h2>
    <div id="listeingredients">
    <?php 
    $ingredients = explode("|", $Recettes[$_GET['recette']]['ingredients']);
    foreach($ingredients as $ingredient){
       ?> <li><?php echo $ingredient;  ?></li>
    <?php 
    }
    ?>
    </div>
    <h2>Preparation : </h2>
    <p><?php echo $Recettes[$_GET['recette']]['preparation'] ?></p>
<?php
}
?>

</main>