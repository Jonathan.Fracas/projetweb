<?php
include "Donnees.inc.php";
include_once "Fonctions/recettes.inc.php";

gestionFavoris($Recettes);


if(!isset($_POST['recherche']) && !isset($_SESSION['recherche'])){
    exit;
}

if(isset($_SESSION['recherche']) && !isset($_POST['recherche'])){
    $_POST['recherche'] = $_SESSION['recherche'];
}

if(isset($_POST['recherche'])){
    $_SESSION['recherche'] = $_POST['recherche'];
}




$listeIngredients = analyseRecherche($_POST['recherche']);


$ingredientsSouhaites = array();
$ingredientsNonSouhaites = array();
$ingredientsNonReconnus = array();
repartitionRecherche($listeIngredients,$ingredientsSouhaites,$ingredientsNonSouhaites,$ingredientsNonReconnus,$Hierarchie);


?>
<nav>
    <?php
        if(!is_array($listeIngredients)){
            ?> 
            <p>Problème de syntaxe dans votre requête : nombre impair de double-quotes </p>
            <?php
        }
        else{
            $dernierIndiceS = dernierIndiceTableau($ingredientsSouhaites);
            $dernierIndiceNS = dernierIndiceTableau($ingredientsNonSouhaites);
            $dernierIndiceNR = dernierIndiceTableau($ingredientsNonReconnus);
        
            if(!empty($ingredientsSouhaites)){
            ?>
               <p>Liste des aliments souhaités : 
                <?php foreach($ingredientsSouhaites as $indice => $ingredient) {
                    if($indice === $dernierIndiceS)
                        echo $ingredient." ";
                    else
                        echo $ingredient.", ";
                    
                }
            }
            ?> </p>

            <?php
            if(!empty($ingredientsNonSouhaites)){
            ?>
               <p>Liste des aliments non souhaités : 
               <?php foreach($ingredientsNonSouhaites as $indice => $ingredient) {
                    if($indice === $dernierIndiceNS)
                        echo $ingredient." ";
                    else
                        echo $ingredient.", ";
                    
                }
            }?> </p>

            <?php
            if(!empty($ingredientsNonReconnus)){
            ?>
               <p>Éléments non reconnus dans la requête : 
               <?php foreach($ingredientsNonReconnus as $indice => $ingredient) {
                    if($indice === $dernierIndiceNR)
                        echo $ingredient." ";
                    else
                        echo $ingredient.", ";
                    
                }
            }?> </p>    
        <?php 
        }
    ?>
</nav>

<main>

<?php

if(!is_array($listeIngredients)){
    ?> 
    <p style="text-align:center">Problème dans votre requête : recherche impossible</p>
    <?php
    exit;
}
   
$listeRecettes = array();
foreach($Recettes as $indice => $Recette){
    $listeRecettes[$indice] = 0;
}


foreach($ingredientsSouhaites as $ingredient){
    foreach($listeRecettes as $indice => $score){
        if($score>=0){
            foreach($Recettes[$indice]['index'] as $ingredientRecette){
                $sousCategorieIngredient = array();
                trouverSousCategories($ingredient,$Hierarchie,$sousCategorieIngredient);
                if(in_array($ingredientRecette,$sousCategorieIngredient)){
                    $listeRecettes[$indice]++;
                    break;
                }
            }
        }
    }
}

foreach($ingredientsNonSouhaites as $ingredient){
    foreach($listeRecettes as $indice => $score){
        foreach($Recettes[$indice]['index'] as $ingredientRecette){
            $sousCategorieIngredient = array();
            trouverSousCategories($ingredient,$Hierarchie,$sousCategorieIngredient);
            if(in_array($ingredientRecette,$sousCategorieIngredient)){
                $listeRecettes[$indice] = -1;
            }
        }
    }
}

arsort($listeRecettes);
$total = count($ingredientsSouhaites)+count($ingredientsNonSouhaites);
if($total == 0){
    ?>
    <p style="text-align:center">Problème dans votre requête : recherche impossible</p>
    <?php
    exit;
}

?>
<div id="recettes">
<?php

    foreach($listeRecettes as $indice => $score){
        if($score>0 || (count($ingredientsSouhaites)==0 && $score>=0)){
?>  
            <div id="recette">
                <div id="nomFav">
<?php         
            echo '
                <ul><a href="?page=recette&recette='.$indice.'">'.$Recettes[$indice]['titre'].' </a></ul>';
            affichageCoeur($indice);
?>
                </div>
                    <div style="text-align: center;">  
<?php
            affichagePhoto($Recettes[$indice]['titre']);
?>
                    </div>
            <p style="font-weight:bold"> <?php  echo ' Score : '.round((($score+count($ingredientsNonSouhaites))/$total*100),2).'%'; ?> </p>
<?php
            foreach($Recettes[$indice]['index'] as $ingredient){
?> 
                <ul><?php  echo $ingredient;?></ul>
<?php
            }
?>
            </div>
<?php 
        }
    }
?>
</div>


</main>