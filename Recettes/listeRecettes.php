<?php

include "Donnees.inc.php";
include "Fonctions/recettes.inc.php";

gestionFavoris($Recettes);


?>

<nav>
    <?php
        include 'Navigation/hierarchie.php';
    ?>
</nav>

<main>

<?php

/**
 * Si l'URL contient un aliment qui n'est pas 'Aliment' 
 * alors on affiche les recettes qui contiennent cette categorie. 
 * sinon on affiche toutes les recettes.
 */
if(isset($_GET['categorie']) && $_GET['categorie'] != 'Aliment' ){
    foreach($Recettes as $indice => $recette){
        foreach($recette['index'] as $ingredient){
            /*
            Pour chaque ingredient de la recette on regarde si celui-ci est dans la categorie selectionne ($_GET['categorie'])
            ou si l'ingredient est une sous categorie de celle-ci.
            */
            if(isset($Hierarchie[$_GET['categorie']]['sous-categorie'])){
                $sousCategories = array(); 
                trouverSousCategories($_GET['categorie'],$Hierarchie,$sousCategories);
                if(in_array($ingredient,$sousCategories)){
                     $listeRecette[$indice] = $recette;    
                }
            }
            else if($_GET['categorie']==$ingredient){
                 $listeRecette[$indice] = $recette;    
            }
 
        }
    }
}
else{
    foreach($Recettes as $indice => $recette){
        $listeRecette[$indice] = $recette;
    }
}

?>
<h2 style="text-align: center;"> Liste des coktails </h2>
<div id="recettes">
<?php
if(isset($listeRecette)){
    foreach($listeRecette as $indice => $recette){
?> 
        <div id="recette">
              <div id="nomFav">
<?php         
        echo '<ul><a href="?page=recette&recette='.$indice.'">'.$recette['titre'].' </a></ul>';
        affichageCoeur($indice);
?>
              </div>
                    <div style="text-align:center">  
<?php
        affichagePhoto($recette['titre']);
?>
                    </div>
<?php
        foreach($recette['index'] as $ingredient){
?> 
            <ul><?php  echo $ingredient;?></ul>
<?php
        }
?>
        </div>
<?php 
    }
}
?>
</div>


</main>

